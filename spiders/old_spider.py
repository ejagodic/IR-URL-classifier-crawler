import scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.linkextractors import LinkExtractor
import langid
from urllib.parse import urlsplit, urlunsplit
import tldextract

from ..items import CrawlerItem

class OldSpider(CrawlSpider):
    name = 'oldspider'
    crawled_urls = {}
    start_urls = [#'https://start-links.nl/',
                #'https://www.startpagina.nl/',
                #'https://suriname.startpagina.nl/',
                #'https://forums.digitalpoint.com/threads/list-of-dutch-directories.845163/',
                'http://odp.org/World/Nederlands/']
    
    rules = (
        Rule(LinkExtractor(allow='Nederlands'), callback='parse_links', follow=True),
        Rule(LinkExtractor(deny='World'), callback='parse_links', follow=True)
        )
    
    def __init__(self, *a, **kw):
        super(OldSpider, self).__init__(*a, **kw)


    def parse_links(self, response):
        o = tldextract.extract(response.request.url)
        domain = o.domain + "." + o.suffix
        #split_url = urlsplit(response.request.url)
        #base_url = split_url.scheme + "://" + split_url.netloc
        if(domain not in self.crawled_urls):
            self.crawled_urls[domain] = True
            item = CrawlerItem()
            item["url"] = response.url
            lang_tag = response.css("html ::attr(lang)").extract()
            if(lang_tag and ('nl' in lang_tag[0].lower() or 'nl' in lang_tag)):
                item["lang"] = "nl"
            else:
                page_text  = ''.join(response.xpath("//text()").extract())

                if(len(page_text) < 128):
                    return

                lang = langid.classify(page_text)
                if(lang[0] != 'nl'):
                    item["lang"] = "other"
                else:
                    item["lang"] = "nl"

            yield item