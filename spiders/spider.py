import scrapy
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.linkextractors import LinkExtractor
import langid
from urllib.parse import urlsplit, urlunsplit
import tldextract

from ..items import CrawlerItem

class SpiderSpider(CrawlSpider):
    name = 'spiderspider'
    crawled_urls = {}
    start_urls = ['http://odp.org/World/Nederlands/']
    allowed_domains = ['odp.org']
    
    rules = (
        Rule(LinkExtractor(allow='Nederlands'), callback='parse_links', follow=True),
        Rule(LinkExtractor(deny='World'), callback='parse_links', follow=True),
        )
    
    def __init__(self, *a, **kw):
        super(SpiderSpider, self).__init__(*a, **kw)

    def parse_links(self, response):
        extractor = LinkExtractor(deny_domains='odp.org')
        links = extractor.extract_links(response)
        item = CrawlerItem()
        for link in links:
            o = tldextract.extract(link.url)
            domain = o.domain + "." + o.suffix
            if(domain not in self.crawled_urls):
                self.crawled_urls[domain] = True
                item['url'] = link.url
                if("Nederlands" in response.url):
                    item['lang'] = "nl"
                else:
                    item['lang'] = "other"
        yield item
        